﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    class TreeNode : IEquatable<TreeNode>
    {
        public Node node;
        public List<List<TreeNode>> child = new List<List<TreeNode>>();
        public TreeNode parent;
        public Guid guid = Guid.NewGuid();

        public bool Equals(TreeNode other)
        {
            if (other == null)
                return false;

            bool answ = true;

            foreach(var ch in child)
            {
                bool OR = false;
                foreach( var oCh in other.child)
                {
                    bool AND = true;
                    foreach (var chItem in ch)
                    {
                        AND &= chItem.Equals(oCh.FirstOrDefault(x => x.node?.Id == chItem.node?.Id));
                    }
                    OR |= AND;
                }
                answ &= OR;
            }

            return answ & other.node?.Id == node?.Id & child.Count() == other.child.Count();
        }

        public override int GetHashCode()
        {
            return node != null? node.Id.GetHashCode(): 0;
        }

    }
}
