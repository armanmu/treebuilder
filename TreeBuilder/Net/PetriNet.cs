﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder.Net
{
    public class PetriNet
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        List<KeyValuePair<Node, Node>> incidence = new List<KeyValuePair<Node, Node>>();
        XmlParser parser;
        TreeNode net = new TreeNode();
        TreeNode tempNet = new TreeNode();
        bool contains = false;

        public void Start(string filename)
        {
            parser = new XmlParser(filename);
            ConsoleKeyInfo keypress;

            do
            {
                incidence = parser.GetIncidence();
                Console.Clear();
                Console.WriteLine("Для выхода нажмите esc");
                Console.ResetColor();
                var index = 0;
                Console.WriteLine("Подсеть - " + parser.pages[index]);
                do
                {
                    keypress = Console.ReadKey();
                    if (keypress.Key == ConsoleKey.DownArrow)
                    {
                        index++;
                        if (index == parser.pages.Count())
                        {
                            index = 0;
                        }
                        Console.Clear();
                        Console.WriteLine("Для выхода нажмите esc");
                        Console.WriteLine("Подсеть - " + parser.pages[index]);
                    }
                } while (keypress.Key != ConsoleKey.Enter);
                var subpage = parser.pages[index];
                index = 0;
                Console.WriteLine("Конечный переход - " + parser.places.Where(x => x.Subnet == subpage).ToList()[index].Name);
                do
                {
                    keypress = Console.ReadKey();
                    if (keypress.Key == ConsoleKey.DownArrow)
                    {
                        index++;
                        if (index == parser.places.Where(x => x.Subnet == subpage).ToList().Count())
                        {
                            index = 0;
                        }
                        Console.Clear();
                        Console.WriteLine("Для выхода нажмите esc");
                        Console.WriteLine("Подсеть - " + subpage);
                        Console.WriteLine("Конечный переход - " + parser.places.Where(x => x.Subnet == subpage).ToList()[index].Name);
                    }
                } while (keypress.Key != ConsoleKey.Enter);
                var name = parser.places.Where(x => x.Subnet == subpage).ToList()[index].Name;
                var root = parser.GetStartNode(name, subpage);
                if (!root.Name.Contains("State"))
                {
                    foreach (var node in incidence.Where(x => x.Value.Id == root.Id).Select(x => x.Value))
                    {
                        node.Marker = false;
                        foreach (var conn in node.ConnectTo)
                        {
                            foreach (var connNode in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                connNode.Marker = false;
                            foreach (var connNode in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                connNode.Marker = false;
                        }
                    }
                    foreach (var node in incidence.Where(x => x.Key.Id == root.Id).Select(x => x.Key))
                    {
                        node.Marker = false;
                        foreach (var conn in node.ConnectTo)
                        {
                            foreach (var connNode in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                connNode.Marker = false;
                            foreach (var connNode in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                connNode.Marker = false;
                        }
                    }
                }
                logger.Info("Построение из " + root.ToString());
                var answ = BuildTree(root, new List<Node>(), new List<Node>(), new List<Node>(), "");
                parser = new XmlParser();
                incidence = parser.GetIncidence();
                //parser.print();
                //net.Update();
                //print(net, "");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Дерево:");
                print(answ.Item1, "");
                var ways = GetWays(answ.Item1);
                foreach (var way in ways)
                {
                    way.RemoveAll(x => x.Type == NodeType.Positon);
                }
                ways = ways.Distinct(new NodeListComparer()).ToList();
                print(ways);
                Console.Read();
                new WaysManager(ways, parser.places, parser.trans, parser.TtoP, parser.PtoT).Trace();
                keypress = Console.ReadKey();
            } while (keypress.Key != ConsoleKey.Escape);
        }

        void print(TreeNode tree, string str)
        {
            if (tree != null)
            {
                Console.ResetColor();
                Console.Write(str);
                if (tree.node?.Type == NodeType.Positon)
                    if (tree.node.Marker)
                        Console.ForegroundColor = ConsoleColor.Green;
                    else
                        Console.ForegroundColor = ConsoleColor.Yellow;
                else
                    Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(tree.node?.Name + " (" + tree.node?.Subnet + ")");
                Console.WriteLine();
                foreach (var ch in tree.child)
                {
                    foreach (var chItem in ch)
                    {
                        print(chItem, str + (ch.Count() > 1 ? "& │" : "  │"));
                    }
                }
            }
        }

        void print(List<List<Node>> ways)
        {
            var i = 1;
            foreach (var way in ways)
            {
                Console.WriteLine();
                Console.WriteLine("Путь №" + i);
                foreach (var step in way)
                    Console.WriteLine(step.Name + " (" + step?.Subnet + ")");
                i++;
            }
        }

        (TreeNode, List<Node>) BuildTree(Node node, List<Node> _neededNode, List<Node> _visited, List<Node> _stopped, string str)
        {
            var stopped = new List<Node>(_stopped);
            var neededNode = new List<Node>(_neededNode);
            var visited = new List<Node>(_visited);
            var currentNode = new TreeNode() { node = node };
            logger.Trace(str + "Зашел в " + node.Type + " " + node.Name + " " + node.Subnet);
            if (visited.Contains(node))
            {
                logger.Trace(str + node.Name + " уже был");
                return (null, neededNode);
            }
            if (stopped.Contains(node))
            {
                logger.Trace(str + node.Name + " заблокирован");

                return (null, neededNode);
            }
            visited.Add(node); //Console.WriteLine(node.Name +" " +node.Subnet);
            if (node.Type == NodeType.Positon)
            {
                if (neededNode.Contains(node))
                {
                    neededNode.RemoveAll(x => x == node);
                    try
                    {
                        foreach (var conn in node.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                neededNode.RemoveAll(x => x == connected);
                        }
                        foreach (var conn in node.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                neededNode.RemoveAll(x => x == connected);
                        }
                    }
                    catch { }
                    logger.Trace(str + node.Name + " удален из необходимых " + neededNode.Count() + " " + (neededNode.Count() == 1 ? neededNode.First().Name : ""));
                }
                if (node.Marker)
                {
                    logger.Trace(str + node.Name + " с маркером, выходим");
                    return (currentNode, neededNode);
                }
                var nn = neededNode;
                foreach (var input in incidence.Where(x => x.Value == node).Select(x => x.Key))
                {
                    var ch = BuildTree(input, neededNode, visited, stopped, str + " ");
                    if (ch.Item1 != null)
                    {
                        currentNode.child.Add(new List<TreeNode>() { ch.Item1 });
                        nn = nn.Except(neededNode.Except(ch.Item2)).ToList();
                    }
                }
                neededNode = nn;
                var subst = new TreeNode();
                if (node.ConnectTo.Count() != 0)
                {
                    foreach (var id in node.ConnectTo.Distinct())
                    {
                        subst = BuildTree(parser.GetNode(id), neededNode, visited, stopped, str + " ").Item1;
                        if (subst != null && subst.child != null)
                        {
                            currentNode.child.AddRange(subst.child);
                        }
                    }
                    currentNode.child = currentNode.child.Distinct().ToList();
                }
                if (currentNode.child.Count != 0)
                    return (currentNode, neededNode);
                else
                    return (null, neededNode);
            }
            else//(node.Type == NodeType.Transaction)
            {
                if (!node.Name.StartsWith("_"))
                    neededNode.AddRange(incidence.Where(x => x.Value == node && !x.Key.Marker).Select(x => x.Key));
                neededNode = neededNode.Distinct().ToList();
                var inpWithMark = incidence.Where(x => x.Value == node && x.Key.Marker).Select(x => x.Key).ToList();
                var inpWithRealMark = incidence.Where(x => x.Value == node && !x.Key.Marker && x.Key.RealMarker).Select(x => x.Key).ToList();
                var inpWithOutMark = incidence.Where(x => x.Value == node && !x.Key.Marker).Select(x => x.Key).ToList();

                var _currentNode = new List<(TreeNode, List<Node>)>();
                foreach (var input in inpWithMark)
                {
                    //цикл по всем оставшимся входным дугам
                    foreach (var inputForStop in incidence.Where(x => x.Value == node).Select(x => x.Key).ToList())
                    {
                        if (inputForStop != input)
                        {
                            stopped.Add(inputForStop);
                            //vis.Marker = false;
                            foreach (var conn in inputForStop.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                {
                                    stopped.Add(connected);
                                    //connected.Marker = false;
                                }

                            }
                            foreach (var conn in inputForStop.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                {
                                    stopped.Add(connected);
                                    //connected.Marker = false;
                                }
                            }
                            logger.Trace(str + inputForStop.Name + " помечен заблокированным");
                        }
                    }
                    //цикл по всем выходным дугам
                    foreach (var output in incidence.Where(x => x.Key == node).Select(x => x.Value).ToList())
                    {
                        try
                        {
                            stopped.Remove(output);
                            if (output != input)
                            {
                                if (inpWithMark.FirstOrDefault(x => x.Id == output.Id) != null)
                                    output.Marker = true;
                                foreach (var conn in output.ConnectTo)
                                {
                                    foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                    {
                                        stopped.Remove(connected);
                                        if (inpWithMark.FirstOrDefault(x => x.Id == output.Id) != null)
                                            connected.Marker = true;
                                    }
                                }
                                foreach (var conn in output.ConnectTo)
                                {
                                    foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                    {
                                        stopped.Remove(connected);
                                        if (inpWithMark.FirstOrDefault(x => x.Id == output.Id) != null)
                                            connected.Marker = true;
                                    }
                                }
                                logger.Trace(str + output.Name + " удален из заблокированных");
                            }
                        }
                        catch { }
                    }
                    //Построение дерева
                    var ch = BuildTree(input, neededNode, visited, stopped, str + " ");
                    if (ch.Item1 != null)
                    {
                        _currentNode.Add(ch);
                    }

                    if (incidence.Where(x => x.Value == input && x.Key == node) == null || incidence.Where(x => x.Value == input && x.Key == node).Count() == 0)
                    {
                        input.Marker = false;
                    }
                    foreach (var vis in incidence.Where(x => x.Value == node).Select(x => x.Key).ToList())
                    {
                        try
                        {
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                    stopped.RemoveAll(x => x == connected);
                            }
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                    stopped.RemoveAll(x => x == connected);
                            }
                            stopped.RemoveAll(x => x == vis);
                        }
                        catch { }
                    }
                }

                foreach (var input in inpWithOutMark)
                {
                    foreach (var vis in incidence.Where(x => x.Value == node).Select(x => x.Key).ToList())
                    {
                        if (vis != input)
                        {
                            stopped.Add(vis);
                            //vis.Marker = false;
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                {
                                    stopped.Add(connected);
                                    //connected.Marker = false;
                                }

                            }
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                {
                                    stopped.Add(connected);
                                    //connected.Marker = false;
                                }
                            }
                            logger.Trace(str + vis.Name + " помечен заблокированным");
                        }
                    }
                    stopped = stopped.Distinct().ToList();
                    foreach (var vis in incidence.Where(x => x.Key == node).Select(x => x.Value).ToList())
                    {
                        try
                        {
                            stopped.RemoveAll(x => x == vis);
                            if (inpWithMark.FirstOrDefault(x => x.Id == vis.Id) != null)
                                vis.Marker = true;
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                                {
                                    stopped.RemoveAll(x => x == connected);
                                    if (inpWithMark.FirstOrDefault(x => x.Id == vis.Id) != null)
                                        connected.Marker = true;
                                }
                            }
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                                {
                                    stopped.RemoveAll(x => x == connected);
                                    if (inpWithMark.FirstOrDefault(x => x.Id == vis.Id) != null)
                                        connected.Marker = true;
                                }
                            }
                            logger.Trace(str + vis.Name + " удален из заблокированных");
                        }
                        catch { }
                    }

                    if (inpWithRealMark.Contains(input) && incidence.Where(x => x.Key == node && x.Value == input).Count() != 0)
                    {
                        input.Marker = true;
                        foreach (var conn in input.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                            {
                                connected.Marker = true;
                            }
                        }
                        foreach (var conn in input.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                            {
                                connected.Marker = true;
                            }
                        }
                    }

                    var ch = BuildTree(input, neededNode, visited, stopped, str + " ");
                    if (ch.Item1 != null)
                    {
                        _currentNode.Add(ch);
                    }

                    if (inpWithRealMark.Contains(input) && incidence.Where(x => x.Key == node && x.Value == input).Count() != 0)
                    {
                        input.Marker = false;
                        foreach (var conn in input.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                            {
                                connected.Marker = false;
                            }
                        }
                        foreach (var conn in input.ConnectTo)
                        {
                            foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                            {
                                connected.Marker = false;
                            }
                        }
                    }
                    foreach (var vis in incidence.Where(x => x.Value == node).Select(x => x.Key).ToList())
                    {
                        try
                        {
                            stopped.RemoveAll(x => x == vis);
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key).ToList())
                                    stopped.RemoveAll(x => x == connected);
                            }
                            foreach (var conn in vis.ConnectTo)
                            {
                                foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value).ToList())
                                    stopped.RemoveAll(x => x == connected);
                            }
                        }
                        catch { }
                    }
                }

                foreach (var input in inpWithMark)
                {
                    input.Marker = true;
                    foreach (var conn in input.ConnectTo)
                    {
                        foreach (var connected in incidence.Where(x => x.Key.Id == conn).Select(x => x.Key))
                        {
                            connected.Marker = true;
                        }
                    }
                    foreach (var conn in input.ConnectTo)
                    {
                        foreach (var connected in incidence.Where(x => x.Value.Id == conn).Select(x => x.Value))
                        {
                            connected.Marker = true;
                        }
                    }
                }

                var child = checkTransInput(_currentNode, incidence.Where(x => x.Value == node && !x.Key.Marker).Select(x => x.Key).ToList());

                if (child.Count != 0 || incidence.Where(x => x.Value == node).Select(x => x.Key).Count() == 0)
                {
                    currentNode.child = child;
                    return (currentNode, _neededNode.Except(incidence.Where(x => x.Value == node && !x.Key.Marker).Select(x => x.Key).ToList()).ToList());
                }
                else
                    return (null, neededNode);
            }
        }

        static List<List<Node>> GetWays(TreeNode tree)
        {
            //List<List<Node>> answ = new List<List<Node>>();
            if (tree.child.Count() == 0)
            {
                var answ = new List<List<Node>>();
                var list = new List<Node>();
                list.Add(tree.node);
                answ.Add(list);
                return answ;
            }
            else
            {
                var answ = new List<List<Node>>();
                foreach (var ch in tree.child)
                {
                    var list = new List<Node>();
                    foreach (var item in ch)
                    {
                        var child = GetWays(item);
                        foreach (var l in child)
                        {
                            list.AddRange(l);
                        }
                    }
                    answ.AddRange(new List<List<Node>> { list });
                }
                foreach (var a in answ)
                {
                    a.Add(tree.node);
                }
                return answ;
            }

        }

        static List<List<TreeNode>> checkTransInput(List<(TreeNode, List<Node>)> trees, List<Node> neededNodes)
        {
            if (trees.Count == 0)
                return new List<List<TreeNode>>();
            if (neededNodes.Intersect(trees.First().Item2).Count() == 0)
            {
                return new List<List<TreeNode>>() { new List<TreeNode>() { trees.First().Item1 } };
            }
            List<List<TreeNode>> answ = new List<List<TreeNode>>();

            foreach (var tree in trees.Skip(1))
            {
                answ.AddRange(checkTransInput(trees.Skip(1).ToList(), neededNodes.Intersect(trees.First().Item2).ToList()));
            }

            foreach (var item in answ)
            {
                item.Add(trees.First().Item1);
            }
            return answ;
        }
    }
}
