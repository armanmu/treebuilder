﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    enum NodeType
    {
        Positon,
        Transaction
    }
    class Node
    {
        public string Id;
        //1-P 2-T
        public NodeType Type;
        public string Name;
        public bool Marker; //{ get { return MarkerCount > 0; } set { } }
        public bool RealMarker;
        //public int MarkerCount;
        public bool newMark = false;
        public string Subnet;
        public List<string> ConnectTo = new List<string>();

        public IMarking marking;
        public Type type;
        public Node()
        {
        }

        public Node(NodeType type, string name, bool marker = false, string subnet = "")
        {
            Type = type;
            Name = name;
            Marker = marker;
            RealMarker = marker;
            Subnet = subnet;
        }

        public Node(string id, NodeType type, string name, bool marker = false, string subnet = ""/*, int markerCount = 0*/)
        {
            Id = id;
            Type = type;
            Name = name;
            Marker = marker;
            RealMarker = marker;
            Subnet = subnet;
            //MarkerCount = markerCount;
        }

        public void CreateMarking(TypeManager typeManager)
        {
            try
            {
                marking = MarkingMaker.Create((dynamic)Activator.CreateInstance(type, null));
                marking.SetTypeManager(typeManager);
            }
            catch
            {
                marking = MarkingMaker.Create((dynamic)Activator.CreateInstance(type, new int[] { 0, 0}));
            }
        }

        public bool Equals(Node other)
        {
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return Name + " (" + Subnet + ")";
        }
    }

    class NodeListComparer : IEqualityComparer<List<Node>>
    {
        public bool Equals(List<Node> x, List<Node> y)
        {
            if (x.Count() != y.Count())
                return false;

            var answ = true;
            for(int i = 0; i < x.Count(); i++)
            {
                answ &= x[i] == y[i];
                if (answ == false)
                    return false;
            }
            return answ;
        }

        public int GetHashCode(List<Node> node)
        {
            var answ = 0;
            foreach(var n in node)
            {
                answ += n.Id.GetHashCode();
            }
            return answ;
        }
    }
}
