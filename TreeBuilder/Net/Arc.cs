﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    class Arc
    {
        public Node TId;
        public Node PId;
        public string guard;
        private List<string> varsList;
        public List<string> vars = new List<string>();
        public bool PtoT;
        public Func<object, bool> func;
        public Func<Dictionary<string, string>, string> outFunc;
        public TypeManager typeManager;

        public Arc(TypeManager typeManager, List<string> varsL, Node place, Node trans, string g, bool ptot)
        {
            this.typeManager = typeManager;
            varsList = varsL;
            PId = place;
            TId = trans;
            guard = g;
            PtoT = ptot;
            foreach (var v in varsList)
                if (guard.Contains(v))
                    vars.Add(v);
            if(PtoT)
            {
                if(guard == "1`()" || guard == "")
                {
                    func = new Func<object, bool>((x) => { return x != null; });
                }
                if (guard.StartsWith("(") && guard.EndsWith(")"))
                {
                    func = new Func<object, bool>((x) => { return x != null; });
                }
            }
            else
            {
                if(guard.StartsWith("if"))
                {
                    outFunc = new Func<Dictionary<string, string>, string>((v) =>
                    {
                        string newG = guard;
                        var text = "";
                        foreach(var val in v)
                        {
                            newG = newG.Replace(val.Key, val.Value);
                        }
                        var rule = String.Concat(newG.Skip(2).Take(newG.IndexOf("then") - 2)).Replace(" ","");

                        if(rule.Split('=')[0] == rule.Split('=')[1])
                        {
                            text = newG.Split(new string[] { "then", "else" }, StringSplitOptions.None)[1];
                        }
                        else
                        {
                            text = newG.Split(new string[] { "then", "else" }, StringSplitOptions.None)[2];
                        }
                        return text;
                    });
                }
                else
                {
                    outFunc = new Func<Dictionary<string, string>, string>((v) =>
                    {
                        var text = guard;
                        foreach (var name in varsList)
                        {

                            if (guard.Contains(name) && v.Select(x => x.Key == name).Count() != 0)
                            {
                                text = text.Replace(v.First(x => x.Key == name).Key, v.First(x => x.Key == name).Value);
                            }
                            else
                            {
                                if (guard.Contains(name))
                                    return null;
                            }

                        }
                        return text;
                    });
                }
            }
        }

        //public List<string> GetVars()
        //{
        //    if()
        //}
    }
}
