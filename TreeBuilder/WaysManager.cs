﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    class WaysManager
    {
        List<List<Node>> ways;
        public List<Node> places = new List<Node>();
        public List<Arc> TtoP = new List<Arc>();
        public List<Arc> PtoT = new List<Arc>();
        public List<Node> trans = new List<Node>();

        public WaysManager(List<List<Node>> _ways, List<Node> _places, List<Node> _trans, List<Arc> _TtoP, List<Arc> _PtoT)
        {                                                             
            ways = _ways;
            places = _places;
            trans = _trans;
            TtoP = _TtoP;
            PtoT = _PtoT;
        }

        public void Trace()
        {
            ConsoleKeyInfo keypress;
            try
            {
                do
                {
                    List<Node> nextStep = ways.Select(x => x.First()).Distinct().ToList();
                    int index = 0;
                    Console.Clear();
                    bool ct = canTrans(nextStep[index].Id);
                    if (ct)
                        Console.ForegroundColor = ConsoleColor.Green;
                    else
                        Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Переход - " + nextStep[index].Name + " (" + nextStep[index].Subnet + ")");

                    do
                    {
                        keypress = Console.ReadKey();
                        if (keypress.Key == ConsoleKey.DownArrow)
                        {
                            index++;
                            if (index == nextStep.Count())
                            {
                                index = 0;
                            }
                            Console.Clear();
                            ct = canTrans(nextStep[index].Id);
                            if (ct)
                                Console.ForegroundColor = ConsoleColor.Green;
                            else
                                Console.ForegroundColor = ConsoleColor.Red;

                            Console.WriteLine("Переход - " + nextStep[index].Name + " (" + nextStep[index].Subnet + ")");
                        }
                    } while (keypress.Key != ConsoleKey.Enter || !ct);
                    complite(nextStep[index].Id);
                    ways.RemoveAll(x => x.First().Id != nextStep[index].Id);
                    foreach (var way in ways)
                    {
                        way.RemoveAt(0);
                    }
                } while (ways.Count() != 0);
            }
            catch(Exception ex)
            { }
            
        }

        bool canTrans(string transId)
        {
            try
            {
                var arcs = PtoT.Where(x => x.TId.Id == transId);
                Dictionary<string, Dictionary<string, Dictionary<string, string>>> markers = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
                List<string> vars = new List<string>();
                foreach (var a in arcs)
                {
                    vars.AddRange(a.vars);
                    var m = places.First(x => x.Id == a.PId.Id).marking.getMarkers(a.guard, a.vars);
                    if (m.Count() == 0)
                        return false;
                    markers.Add(a.PId.Id, m);
                }
                vars = vars.Distinct().ToList();

                if (vars.Count() == 0)
                {
                    if (markers.Where(x => x.Value.Count() == 0).Count() != 0)
                        return false;
                    else return true;
                }
                Dictionary<string, List<string>> variants = new Dictionary<string, List<string>>();

                foreach (var v in vars)
                {
                    List<string> values = new List<string>();
                    var locValues = new List<string>();

                    foreach (var mard1 in markers)
                    {
                        locValues = new List<string>();
                        foreach (var marD in mard1.Value)
                        {
                            if (marD.Value.ContainsKey(v))
                            {
                                locValues.Add(marD.Value[v]);
                            }
                        }
                        if (values.Count() == 0)
                        {
                            values.AddRange(locValues);
                        }
                        else
                        {
                            if (locValues.Count() != 0)
                                values = values.Intersect(locValues).ToList();
                        }
                    }

                    if (values.Count == 0)
                        return false;
                    variants.Add(v, values);
                }

                Dictionary<string, Dictionary<string, Dictionary<string, string>>> markersUpdate = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();


                foreach (var v in variants)
                {
                    foreach (var mard1 in markers)
                    {
                        if (!markersUpdate.ContainsKey(mard1.Key))
                            markersUpdate.Add(mard1.Key, mard1.Value.Where(x => !x.Value.ContainsKey(v.Key) || v.Value.Contains(x.Value[v.Key])).ToDictionary(x => x.Key, x => x.Value));
                        else
                            markersUpdate[mard1.Key] = markersUpdate[mard1.Key].Where(x => !x.Value.ContainsKey(v.Key) || v.Value.Contains(x.Value[v.Key])).ToDictionary(x => x.Key, x => x.Value);
                    }
                }

                variants = new Dictionary<string, List<string>>();

                foreach (var v in vars)
                {
                    List<string> values = new List<string>();
                    var locValues = new List<string>();

                    foreach (var mard1 in markersUpdate)
                    {
                        locValues = new List<string>();
                        foreach (var marD in mard1.Value)
                        {
                            if (marD.Value.ContainsKey(v))
                            {
                                locValues.Add(marD.Value[v]);
                            }
                        }
                        if (values.Count() == 0)
                        {
                            values.AddRange(locValues);
                        }
                        else
                        {
                            if (locValues.Count() != 0)
                                values = values.Intersect(locValues).ToList();
                        }
                    }

                    if (values.Count == 0)
                        return false;
                    variants.Add(v, values);
                }
                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
            
        }

        void complite(string transId)
        {
            var arcs = PtoT.Where(x => x.TId.Id == transId);
            var arcsOut = TtoP.Where(x => x.TId.Id == transId);
            var varValues = new Dictionary<string, string>();

            Dictionary<string, Dictionary<string, Dictionary<string, string>>> markers = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();
            List<string> vars = new List<string>();
            foreach (var a in arcs)
            {
                vars.AddRange(a.vars);
                var m = places.First(x => x.Id == a.PId.Id).marking.getMarkers(a.guard, a.vars);
                if (m.Count() == 0)
                    return;
                markers.Add(a.PId.Id, m);
            }
            vars = vars.Distinct().ToList();

            if(vars.Count() != 0)
            {
                Dictionary<string, List<string>> variants = new Dictionary<string, List<string>>();

                foreach (var v in vars)
                {
                    List<string> values = new List<string>();
                    var locValues = new List<string>();

                    foreach (var mard1 in markers)
                    {
                        locValues = new List<string>();
                        foreach (var marD in mard1.Value)
                        {
                            if (marD.Value.ContainsKey(v))
                            {
                                locValues.Add(marD.Value[v]);
                            }
                        }
                        if (values.Count() == 0)
                        {
                            values.AddRange(locValues);
                        }
                        else
                        {
                            if (locValues.Count() != 0)
                                values = values.Intersect(locValues).ToList();
                        }
                    }

                    if (values.Count == 0)
                        return;
                    variants.Add(v, values);
                }

                Dictionary<string, Dictionary<string, Dictionary<string, string>>> markersUpdate = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();


                foreach (var v in variants)
                {
                    foreach (var mard1 in markers)
                    {
                        if (!markersUpdate.ContainsKey(mard1.Key))
                            markersUpdate.Add(mard1.Key, mard1.Value.Where(x => !x.Value.ContainsKey(v.Key) || v.Value.Contains(x.Value[v.Key])).ToDictionary(x => x.Key, x => x.Value));
                        else
                            markersUpdate[mard1.Key] = markersUpdate[mard1.Key].Where(x => !x.Value.ContainsKey(v.Key) || v.Value.Contains(x.Value[v.Key])).ToDictionary(x => x.Key, x => x.Value);
                    }
                }

                variants = new Dictionary<string, List<string>>();

                foreach (var v in vars)
                {
                    List<string> values = new List<string>();
                    var locValues = new List<string>();

                    foreach (var mard1 in markersUpdate)
                    {
                        locValues = new List<string>();
                        foreach (var marD in mard1.Value)
                        {
                            if (marD.Value.ContainsKey(v))
                            {
                                locValues.Add(marD.Value[v]);
                            }
                        }
                        if (values.Count() == 0)
                        {
                            values.AddRange(locValues);
                        }
                        else
                        {
                            if (locValues.Count() != 0)
                                values = values.Intersect(locValues).ToList();
                        }
                    }

                    if (values.Count == 0)
                        return;
                    variants.Add(v, values);
                }

                foreach (var v in variants)
                {
                    if (v.Value.Count() > 1)
                    {
                        ConsoleKeyInfo keypress;
                        int index = 0;
                        Console.Clear();
                        Console.WriteLine("Выбор значения переменной " + v.Key);
                        do
                        {
                            keypress = Console.ReadKey();
                            if (keypress.Key == ConsoleKey.DownArrow)
                            {
                                index++;
                                if (index == v.Value.Count())
                                {
                                    index = 0;
                                }
                                Console.Clear();
                                Console.WriteLine("Выбор значения переменной " + v.Key);
                                Console.WriteLine(v.Value[index]);
                            }
                        } while (keypress.Key != ConsoleKey.Enter);
                        varValues.Add(v.Key, v.Value[index]);

                    }
                    else
                    {
                        varValues.Add(v.Key, v.Value.First());
                    }
                }

                markers = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

                foreach (var v in varValues)
                {
                    foreach (var mard1 in markersUpdate)
                    {
                        if (!markers.ContainsKey(mard1.Key))
                            markers.Add(mard1.Key, mard1.Value.Where(x => !x.Value.ContainsKey(v.Key) || v.Value == x.Value[v.Key]).ToDictionary(x => x.Key, x => x.Value));
                        else
                            markers[mard1.Key] = markers[mard1.Key].Where(x => !x.Value.ContainsKey(v.Key) || v.Value == x.Value[v.Key]).ToDictionary(x => x.Key, x => x.Value);
                    }
                }


            }

            Console.ResetColor();
            Console.WriteLine("Маркировка до");
            foreach(var a in arcs)
            {
                Console.WriteLine("Позиция " + places.First(x => x.Id == a.PId.Id).ToString() +" маркировка "+ places.First(x => x.Id == a.PId.Id).marking.ToString());
            }

            foreach (var a in arcsOut)
            {
                Console.WriteLine("Позиция " + places.First(x => x.Id == a.PId.Id).ToString() + " маркировка " + places.First(x => x.Id == a.PId.Id).marking.ToString());
            }

            foreach (var m in markers)
            {
                places.First(x => x.Id == m.Key).marking.GetMarker(m.Value.Keys.First());
            }

            foreach(var a in arcsOut)
            {
                var text = a.outFunc.Invoke(varValues);
                var count = 1;
                if (text.Contains('`'))
                {
                    count = Int32.Parse(text.Split('`')[0]);
                    text = String.Concat(text.Skip(text.IndexOf('`')+1));
                }

                for (int i = 0; i < count; i++)
                {
                    places.First(x => x.Id == a.PId.Id).marking.AddMarker(a.typeManager.Convert(text, a.PId.type));
                    places.First(x => x.Id == a.PId.Id).marking.Registr();
                }
            }

            Console.WriteLine();
            Console.WriteLine("Маркировка после");
            foreach (var a in arcs)
            {
                Console.WriteLine("Позиция " + places.First(x => x.Id == a.PId.Id).ToString() + " маркировка " + places.First(x => x.Id == a.PId.Id).marking.ToString());
            }

            foreach (var a in arcsOut)
            {
                Console.WriteLine("Позиция " + places.First(x => x.Id == a.PId.Id).ToString() + " маркировка " + places.First(x => x.Id == a.PId.Id).marking.ToString());
            }

            Console.ReadKey();
        }
    }
}
