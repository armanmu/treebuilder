﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    public interface IMarking
    {
        void AddMarker(object marker);
        void Registr();
        void GetMarker(string marker);
        int GetMarkersCount();
        bool Contains(object marker);
        Dictionary<string, Dictionary<string, string>> getMarkers(string guard, List<string> vars);
        void SetTypeManager(TypeManager typeManager);
    }
}
