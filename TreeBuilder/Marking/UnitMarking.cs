﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    class UnitMarking : IMarking
    {
        public int Markers { get; private set; }

        public UnitMarking(int count = 0)
        {
            Markers = count;
        }
        public bool AddMarker()
        {
            Markers++;
            return true;
        }

        public bool AddMarker(int marker)
        {
            Markers++;
            return true;
        }

        public void AddMarker(object marker)
        {
            throw new NotImplementedException();
        }

        public void Registr()
        {
            throw new NotImplementedException();
        }

        public int GetMarkersCount()
        {
            throw new NotImplementedException();
        }

        public bool Contains(object marker)
        {
            throw new NotImplementedException();
        }

        public void GetMarker(string marker)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, Dictionary<string, string>> getMarkers(string guard, List<string> vars)
        {
            throw new NotImplementedException();
        }

        public void SetTypeManager(TypeManager typeManager)
        {
            throw new NotImplementedException();
        }
    }
}
