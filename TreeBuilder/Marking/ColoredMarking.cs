﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    public static class MarkingMaker
    {
        public static ColoredMarking<T> Create<T> (T obj)
        {
            return new ColoredMarking<T>();
        }
    }
    public class ColoredMarking<T> : IMarking
    {
        T newMarker;
        TypeManager typeManager;

        public override string ToString()
        {
            var str = "";
            foreach (var m in Markers)
            {
                str += m.Value.ToString() + "`" + m.Key.ToString() + " ++";
            }
            return str;
        }

        public ColoredMarking()
        {
            Markers = new Dictionary<T, int>();
        }

        public ColoredMarking(Dictionary<T, int> marking)
        {
            this.Markers = marking;
        }

        public Dictionary<T, int> Markers { get; private set; }

        public bool AddMarker(T marker)
        {
            try
            {
                if (Markers.ContainsKey(marker))
                {
                    Markers[marker]++;
                }
                else
                {
                    Markers.Add(marker, 1);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddMarker()
        {
            throw new NotImplementedException();
        }

        public void AddMarker(object marker)
        {
            newMarker = (T)marker;
        }

        public bool Contains(object marker)
        {
            throw new NotImplementedException();
        }

        public void GetMarker(string marker)
        {
            Markers[(T)typeManager.Convert(marker, typeof(T))]--;
            if(Markers[(T)typeManager.Convert(marker, typeof(T))] == 0)
                Markers.Remove((T)typeManager.Convert(marker, typeof(T)));
        }

        public Dictionary<string, Dictionary<string, string>> getMarkers(string guard, List<string> vars)
        {
            Dictionary<string, Dictionary<string, string>> answ = new Dictionary<string, Dictionary<string, string>>();
            if (typeof(T) == typeof(Product2))
            {
                var text = guard.Replace("(", "").Replace(")", "").Replace(" ", "").Split(',');

                if(vars.Contains(text[0]))
                {
                    if(vars.Contains(text[1]))
                    {
                        foreach(var m in Markers.Keys)
                        {
                            answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product2).Item1.ToString() }, { text[1], (m as Product2).Item2.ToString() } });
                        }
                    }
                    else
                    {
                        foreach (var m in Markers.Keys.Where(x => (x as Product2).Item2.ToString() == text[1]))
                        {
                            answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product2).Item1.ToString()}});
                        }
                    }
                }
                else
                {
                    if (vars.Contains(text[1]))
                    {
                        foreach (var m in Markers.Keys.Where(x => (x as Product2).Item1.ToString() == text[0]))
                        {
                            answ.Add(m.ToString(), new Dictionary<string, string>() { { text[1], (m as Product2).Item2.ToString() } });
                        }
                    }
                    else
                    {
                        foreach (var m in Markers.Keys.Where(x => (x as Product2).Item1.ToString() == text[0] && (x as Product2).Item2.ToString() == text[1]))
                        {
                            answ.Add(m.ToString(), new Dictionary<string, string>());
                        }
                    }
                }
            }
            else
            {
                if(typeof(T) == typeof(Product3))
                {
                    var text = guard.Replace("(", "").Replace(")", "").Replace(" ", "").Split(',');

                    if (vars.Contains(text[0]))
                    {
                        if (vars.Contains(text[1]))
                        {
                            if(vars.Contains(text[2]))
                            {
                                foreach (var m in Markers.Keys)
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product3).Item1.ToString() }, { text[1], (m as Product3).Item2.ToString() }, { text[2], (m as Product3).Item3.ToString() } });
                                }
                            }
                            else
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item3.ToString() == text[2]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product3).Item1.ToString() }, { text[1], (m as Product3).Item2.ToString() } });
                                }
                            }
                        }
                        else
                        {
                            if (vars.Contains(text[2]))
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item2.ToString() == text[1]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product3).Item1.ToString() }, { text[2], (m as Product3).Item3.ToString() } });
                                }
                            }
                            else
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item3.ToString() == text[2]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[0], (m as Product3).Item1.ToString() } });
                                }
                            }
                        }
                    }
                    else
                    {
                        if (vars.Contains(text[1]))
                        {
                            if (vars.Contains(text[2]))
                            {
                                foreach (var m in Markers.Keys)
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[1], (m as Product3).Item2.ToString() }, { text[2], (m as Product3).Item3.ToString() } });
                                }
                            }
                            else
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item3.ToString() == text[2]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[1], (m as Product3).Item2.ToString() } });
                                }
                            }
                        }
                        else
                        {
                            if (vars.Contains(text[2]))
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item2.ToString() == text[1]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>() { { text[2], (m as Product3).Item3.ToString() } });
                                }
                            }
                            else
                            {
                                foreach (var m in Markers.Keys.Where(x => (x as Product3).Item3.ToString() == text[2]))
                                {
                                    answ.Add(m.ToString(), new Dictionary<string, string>());
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (vars.Contains(guard))
                    {
                        foreach (var m in Markers.Keys)
                        {
                            answ.Add(m.ToString(), new Dictionary<string, string>() { { guard, m.ToString() } });
                        }
                    }
                    else
                    {
                        if(guard == string.Empty)
                        {
                            foreach (var m in Markers.Keys)
                            {
                                answ.Add(m.ToString(), new Dictionary<string, string>());
                            }
                        }
                        else
                        {
                            if (Markers.ContainsKey((T)typeManager.Convert(guard, typeof(T))))
                            {
                                answ.Add(guard, new Dictionary<string, string>());
                            }
                        }

                    }
                }
            }
            return answ;
        }

        public int GetMarkersCount()
        {
            return Markers.Count();
        }

        public void Registr()
        {
            AddMarker(newMarker);
        }

        public void SetTypeManager(TypeManager typeManager)
        {
            this.typeManager = typeManager;
        }
    }
}
