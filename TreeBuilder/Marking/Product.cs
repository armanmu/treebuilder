﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    public class Product2
    {
        public int Item1;
        public int Item2;

        public Product2()
        { }

        public override int GetHashCode()
        {
            return Item1.GetHashCode() & Item2.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return (obj as Product2).Item1 == Item1 && (obj as Product2).Item2 == Item2;
        }

        public override string ToString()
        {
            return "(" + Item1.ToString() + "," + Item2.ToString() +")";
        }
    }

    public class Product3
    {
        public int Item1;
        public int Item2;
        public int Item3;

        public Product3()
        { }

        public override int GetHashCode()
        {
            return Item1.GetHashCode() & Item2.GetHashCode() & Item3.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return (obj as Product3).Item1 == Item1 && (obj as Product3).Item2 == Item2 && (obj as Product3).Item3 == Item3;
        }

        public override string ToString()
        {
            return "(" + Item1.ToString() + "," + Item2.ToString() + "," + Item3.ToString() + ")";
        }
    }
}
