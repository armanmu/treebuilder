﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreeBuilder.Net;

namespace TreeBuilder
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            while(true)
            {
                System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    new PetriNet().Start(ofd.FileName);
                }
            }
        }
    }
}
