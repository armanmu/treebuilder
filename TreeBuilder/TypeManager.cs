﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeBuilder
{
    public class TypeManager
    {
        public Dictionary<string, string> Types = new Dictionary<string, string>();
        public Type GetType(string typeName)
        {
            switch (typeName)
            {
                case "INT": return typeof(Int32);
                case "UNIT": return typeof(Unit);
                default: return SearchType(typeName);
            }
        }

        public object Convert(string value, Type type)
        {
            if (value == "empty")
                return null;
            if(type == typeof(Product2))
            {
                var val = value.Replace("(","").Replace(")","").Split(',');
                return new Product2() { Item1 = int.Parse(val[0]), Item2 = int.Parse(val[1]) };
            }
            if(type == typeof(Product3))
            {
                var val = value.Replace("(", "").Replace(")", "").Split(',');
                return new Product3() { Item1 = int.Parse(val[0]), Item2 = int.Parse(val[1]), Item3 = int.Parse(val[2]) };
            }
            if(type == typeof(Unit))
            {
                return new Unit();
            }

            return System.Convert.ChangeType(value, type);
        }
        private Type SearchType(string typeName)
        {
            if(Types.ContainsKey(typeName))
            {
                var types = Types[typeName].Split('*');
                switch (types.Length)
                {
                    case 2: return typeof(Product2);
                    case 3: return typeof(Product3);
                }
            }
            return typeof(object);
        }
    }
}
