﻿using System;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TreeBuilder
{
    class XmlParser
    {
        public List<KeyValuePair<Node, Node>> incidence = new List<KeyValuePair<Node, Node>>();
        static List<KeyValuePair<string, string>> connects = new List<KeyValuePair<string, string>>();
        public List<KeyValuePair<string, Type>> vars = new List<KeyValuePair<string, Type>>();
        public List<KeyValuePair<string, string>> vals = new List<KeyValuePair<string, string>>();
        public List<Node> places = new List<Node>();
        public List<Arc> TtoP = new List<Arc>();
        public List<Arc> PtoT = new List<Arc>();
        public List<Node> trans = new List<Node>();
        public List<string> pages = new List<string>();
        public TypeManager typeManager = new TypeManager();
        private string fileName;

        public XmlParser(string fileName)
        {
            incidence = new List<KeyValuePair<Node, Node>>();
            connects = new List<KeyValuePair<string, string>>();
            vars = new List<KeyValuePair<string, Type>>();
            vals = new List<KeyValuePair<string, string>>();
            places = new List<Node>();
            TtoP = new List<Arc>();
            PtoT = new List<Arc>();
            trans = new List<Node>();
            pages = new List<string>();
            typeManager = new TypeManager();
            this.fileName = fileName;
            Init();
        }

        private void Init()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(fileName);
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            // обход всех узлов в корневом элементе
            foreach (XmlNode xnode in xRoot)
            {
                if (xnode.Name == "cpnet")
                {
                    foreach (XmlNode ch in xnode.ChildNodes)
                    {
                        if(ch.Name == "globbox")
                        {
                            foreach(XmlNode colorBlock in ch.ChildNodes)
                            {
                                if(colorBlock.FirstChild.InnerText == "Standard declarations")
                                {
                                    foreach(XmlNode color in colorBlock.ChildNodes)
                                    {
                                        if(color.ChildNodes.Count > 1 && color.ChildNodes.Item(1).Name == "product")
                                        {
                                            typeManager.Types.Add(color.FirstChild.InnerText, color.LastChild.InnerText.Split(' ').Last());
                                        }
                                        
                                        if(color.Name == "var")
                                        {
                                            Type type = typeManager.GetType(color.FirstChild.FirstChild.InnerText);
                                            foreach(XmlNode v in color.ChildNodes)
                                            {
                                                if(v.Name == "id")
                                                {
                                                    vars.Add(new KeyValuePair<string, Type>(v.InnerText, type));
                                                }
                                            }
                                        }

                                        if(color.Name == "ml")
                                        {
                                            var text = color.InnerText.Remove(color.InnerText.IndexOf(';')).Replace("val ", "").Replace(";", "").Replace(" ", "").Split('=');
                                            vals.Add(new KeyValuePair<string, string>(text[0], text[1]));
                                        }
                                    }
                                }
                            }
                        }
                        if (ch.Name == "page")
                        {
                            string pageName = "";
                            foreach (XmlNode node in ch.ChildNodes)
                            {
                                if (node.Name == "pageattr")
                                {
                                    pageName = node.Attributes.GetNamedItem("name").Value.Replace(" ", "");
                                    if (!pageName.StartsWith("_"))
                                            pages.Add(pageName);
                                }
                                if (!pageName.StartsWith("_"))
                                {
                                    if (node.Name == "place")
                                    {
                                        var attr = node.Attributes;
                                        string placeName = "";
                                        bool marker = false;
                                        Type type = null;
                                        string[] markerStrings = new List<string>().ToArray();
                                        foreach (XmlNode atr in node.ChildNodes)
                                        {
                                            if (atr.Name == "type")
                                            {
                                                type = typeManager.GetType(atr.LastChild.InnerText);
                                            }
                                            if (atr.Name == "initmark")
                                            {
                                                foreach (XmlNode mark in atr.ChildNodes)
                                                {
                                                    if (mark.Name == "text")
                                                    {
                                                        markerStrings = Regex.Replace(mark.InnerText.Replace("++", "+"), @"[ \r\n\t]", "").Split('+');    
                                                        
                                                        marker = !String.IsNullOrEmpty(mark.InnerText);
                                                    }
                                                }
                                            }

                                            if (atr.Name == "text")
                                            {
                                                placeName = atr.InnerText;
                                            }
                                        }
                                        var place = new Node(attr.GetNamedItem("id").Value, NodeType.Positon, Regex.Replace(placeName, @"[ \r\n\t]", ""), marker, pageName);
                                        place.type = type;
                                        place.CreateMarking(typeManager);
                                        if(marker)
                                        {
                                            foreach (var markStr in markerStrings)
                                            {
                                                try
                                                {
                                                    for (int i = 0; i < Int32.Parse(markStr.Split('`')[0]); i++)
                                                    {
                                                        place.marking.AddMarker(typeManager.Convert(markStr.Split('`')[1], type));
                                                        place.marking.Registr();
                                                    }
                                                }
                                                catch { }
                                            }
                                        }
                                       
                                        var con = connects.Where(x => x.Key == attr.GetNamedItem("id").Value);
                                        if (con != null && con.Count() != 0)
                                        {
                                            foreach (var sock in con)
                                            {
                                                place.ConnectTo.Add(sock.Value);
                                                place.Marker = places.First(x => x.Id == sock.Value).Marker;
                                                //place.MarkerCount = places.First(x => x.Id == sock.Value).MarkerCount;
                                            }
                                        }
                                        places.Add(place);
                                    }

                                    if (node.Name == "trans")
                                    {
                                        var attr = node.Attributes;
                                        string transName = "";
                                        bool marker = false;
                                        foreach (XmlNode atr in node.ChildNodes)
                                        {
                                            if (atr.Name == "text")
                                            {
                                                transName = atr.InnerText;
                                            }
                                            if (atr.Name == "subst" && !transName.StartsWith("_"))
                                            {
                                                var conn = atr.Attributes.GetNamedItem("portsock").Value;
                                                conn = conn.Replace(")(", ";").Replace("(", "").Replace(")", "");
                                                foreach (var sock in conn.Split(';'))
                                                    connects.Add(new KeyValuePair<string, string>(sock.Split(',')[0], sock.Split(',')[1]));
                                            }
                                        }
                                        trans.Add(new Node(attr.GetNamedItem("id").Value, NodeType.Transaction, Regex.Replace(transName, @"[ \r\n]", " "), marker, pageName));
                                    }

                                    if (node.Name == "arc")
                                    {
                                        var orient = node.Attributes.GetNamedItem("orientation").Value;
                                        string tId = "";
                                        string pId = "";
                                        foreach (XmlNode atr in node.ChildNodes)
                                        {
                                            if (atr.Name == "transend")
                                            {
                                                tId = atr.Attributes.GetNamedItem("idref").Value;
                                            }
                                            if (atr.Name == "placeend")
                                            {
                                                pId = atr.Attributes.GetNamedItem("idref").Value;
                                            }
                                        }
                                        var text = Regex.Replace(node.LastChild.LastChild.InnerText, @"[ \r\n]", "");
                                        foreach(var v in vals)
                                        {
                                            text = text.Replace(v.Key, v.Value);
                                        }
                                        if (orient == "PtoT")
                                        {
                                            incidence.Add(new KeyValuePair<Node, Node>(places.First(x => x.Id == pId), trans.First(x => x.Id == tId)));
                                            PtoT.Add(new Arc(typeManager, vars.Select(x => x.Key).ToList(), places.First(x => x.Id == pId), trans.First(x => x.Id == tId), text, true ));
                                        }                                                                                                                                  
                                        if (orient == "TtoP")                                                                                                              
                                        {                                                                                                                                  
                                            incidence.Add(new KeyValuePair<Node, Node>(trans.First(x => x.Id == tId), places.First(x => x.Id == pId)));                    
                                            TtoP.Add(new Arc(typeManager, vars.Select(x => x.Key).ToList(), places.First(x => x.Id == pId), trans.First(x => x.Id == tId), text, false));
                                        }                                                                                                                                  
                                        if (orient == "BOTHDIR")                                                                                                           
                                        {                                                                                                                                  
                                            incidence.Add(new KeyValuePair<Node, Node>(places.First(x => x.Id == pId), trans.First(x => x.Id == tId)));                    
                                            incidence.Add(new KeyValuePair<Node, Node>(trans.First(x => x.Id == tId), places.First(x => x.Id == pId)));                    
                                            PtoT.Add(new Arc(typeManager, vars.Select(x => x.Key).ToList(), places.First(x => x.Id == pId), trans.First(x => x.Id == tId), text, true));
                                            TtoP.Add(new Arc(typeManager, vars.Select(x => x.Key).ToList(), places.First(x => x.Id == pId), trans.First(x => x.Id == tId), text, false));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                foreach(var con in connects)
                {
                    foreach (var _node in incidence.Where(x => x.Key.Id == con.Value).Select(x => x.Key))
                    {
                        _node.ConnectTo.Add(con.Key);
                        _node.ConnectTo = _node.ConnectTo.Distinct().ToList();
                    }
                    foreach (var _node in incidence.Where(x => x.Value.Id == con.Value).Select(x => x.Value))
                    {
                        _node.ConnectTo.Add(con.Key);
                        _node.ConnectTo = _node.ConnectTo.Distinct().ToList();
                    }
                    foreach (var _node in places.Where(x => x.Id == con.Value))
                    {
                        _node.ConnectTo.Add(con.Key);
                        _node.ConnectTo = _node.ConnectTo.Distinct().ToList();
                    }
                    //Console.WriteLine(places.First(x => x.Id == con.Value).marking.GetMarkersCount().ToString() + " " + places.First(x => x.Id == con.Key).marking.GetMarkersCount().ToString());
                    places.First(x => x.Id == con.Key).marking = places.First(x => x.Id == con.Value).marking;
                    //Console.WriteLine(places.First(x => x.Id == con.Value).marking.GetMarkersCount().ToString() + " " + places.First(x => x.Id == con.Key).marking.GetMarkersCount().ToString());
                }
            }
        }

        public List<KeyValuePair<Node, Node>> GetIncidence()
        {
            return incidence;
        }

        public Node GetStartNode(string Name, string Page)
        {
            return places.First(x => x.Name == Name && x.Subnet == Page);
        }

        public Node GetNode(string Id)
        {
            return places.First(x => x.Id == Id);
        }

        public void print()
        {
            foreach(var p in places)
            {
                Console.WriteLine(p.Name + " " + p.Subnet);
                foreach (var c in p.ConnectTo)
                {
                    Console.WriteLine("   "+GetNode(c).Name + " " + GetNode(c).Subnet);
                }
            }
        }
    }
}
